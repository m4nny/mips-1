# my\_mips

The my\_mips project is a MIPS-1 compliant user-land emulator. It has been
realised for educational purpose only. It is, however, partially functional.

It provides, among other things:

* Handling of both striped binaries (only .text segment,) and MIPS ELF32 binary
  files.

* A small set of operating-system-like services through the use of the
  'syscall' instructiobn (see the list below.)

* A subset of the MIPS-1 instructions, excluding in particular floating point
  instructions, which list you can see below.

* A debug feature, both printing instructions and/or registers content at
  each instruction call.


## Available instructions

### Arithmetics

* add
* addu
* addi
* addiu
* div
* divu
* mult
* multu
* sub
* subu

### Logic

* and
* andi
* nor
* or
* ori
* slt
* sltu
* slti
* sltui
* xor
* xori

### Shifts

* sll
* sllv
* srl
* srlv

### Loads and stores

* lb
* lbu
* lh
* lhu
* lw
* sb
* sh
* sw
* lui
* mfhi
* mflo
* mthi
* mtlo

### Jumps and branches

* beq
* bgez
* bgezal
* bgtz
* blez
* bltz
* bltzal
* j
* jal
* jalr
* jr

## Available syscalls

* print\_int
* print\_string
* read\_int
* read\_string
* exit
* print\_char
* read\_char
* open
* read
* write
* close
* exit2
