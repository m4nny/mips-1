#ifndef RUN_HH_
# define RUN_HH_

# include <boost/cstdint.hpp>

# include <debug.hh>
# include <env/memory.hh>
# include <env/registers.hh>
# include <instr/instruction.hh>


int run();


#endif /* !RUN_HH_ */
