#include <instr/branch.hh>


void
Branch::jalr(uint32_t ins)
{
    if (DBG_INS(g_debug))
        fprintf(stderr, "jalr $%u, $%u", GRD(ins), GRS(ins));

    g_reg->set(GRD(ins), g_reg->getpc() + 4);
    g_reg->setpc(g_reg->get(GRS(ins)));
}


void
Branch::jal(uint32_t ins)
{
    uint32_t address((GAD(ins) << 2) + (g_reg->getpc() & 0xF0000000));

    if (DBG_INS(g_debug))
        fprintf(stderr, "jal 0x%.8X\n", address);

    g_reg->set(RA, g_reg->getpc() + 4);
    g_reg->setpc(address);
}


void
Branch::jr(uint32_t ins)
{
    if (DBG_INS(g_debug))
        fprintf(stderr, "jr $%u\n", GRS(ins));
    g_reg->setpc(g_reg->get(GRS(ins)));
}


void
Branch::j(uint32_t ins)
{
    uint32_t address((GAD(ins) << 2) + (g_reg->getpc() & 0xF0000000));

    if (DBG_INS(g_debug))
        fprintf(stderr, "j 0x%.8X\n", address);

    g_reg->setpc(address);
}


void
Branch::beq(uint32_t ins)
{
    int32_t offset(GIM(ins) << 2);

    if (DBG_INS(g_debug))
        fprintf(stderr, "beq $%u, $%u, 0x%.8X\n", GRS(ins), GRT(ins), offset);

    if (g_reg->get(GRS(ins)) == g_reg->get(GRT(ins)))
        g_reg->setpc(offset + g_reg->getpc());
}


void
Branch::bne(uint32_t ins)
{
    int32_t offset(GIM(ins) << 2);

    if (DBG_INS(g_debug))
        fprintf(stderr, "bne $%u, $%u, 0x%.8X\n", GRS(ins), GRT(ins), offset);

    if (g_reg->get(GRS(ins)) != g_reg->get(GRT(ins)))
        g_reg->setpc(offset + g_reg->getpc());
}


void
Branch::regimm(uint32_t ins)
{
    int32_t offset(GIM(ins) << 2);

    switch (GRT(ins))
    {
        case BGEZ:
            if (DBG_INS(g_debug))
                fprintf(stderr, "bgez $%u, 0x%.8X\n", GRS(ins), offset);
            if (g_reg->get(GRS(ins)) >= 0)
                g_reg->setpc(offset + g_reg->getpc());
            break;
        case BGEZAL:
            if (DBG_INS(g_debug))
                fprintf(stderr, "bgezal $%u, 0x%.8X\n", GRS(ins), offset);
            g_reg->set(RA, g_reg->getpc() + 4);
            if (g_reg->get(GRS(ins)) >= 0)
                g_reg->setpc(offset + g_reg->getpc());
            break;
        case BLTZ:
            if (DBG_INS(g_debug))
                fprintf(stderr, "bltz $%u, 0x%.8X\n", GRS(ins), offset);
            if (g_reg->get(GRS(ins)) < 0)
                g_reg->setpc(offset + g_reg->getpc());
            break;
        case BLTZAL:
            if (DBG_INS(g_debug))
                fprintf(stderr, "bltzal $%u, 0x%.8X\n", GRS(ins), offset);
            g_reg->set(RA, g_reg->getpc() + 4);
            if (g_reg->get(GRS(ins)) < 0)
                g_reg->setpc(offset + g_reg->getpc());
            break;
    }
}


void
Branch::bgtz(uint32_t ins)
{
    int32_t offset(GIM(ins) << 2);

    if (DBG_INS(g_debug))
        fprintf(stderr, "bgtz $%u, 0x%.8X\n", GRS(ins), offset);

    if (g_reg->get(GRS(ins)) > 0)
        g_reg->setpc(offset + g_reg->getpc());
}


void
Branch::blez(uint32_t ins)
{
    int32_t offset(GIM(ins) << 2);

    if (DBG_INS(g_debug))
        fprintf(stderr, "blez $%u, 0x%.8X\n", GRS(ins), offset);

    if (g_reg->get(GRS(ins)) <= 0)
        g_reg->setpc(offset + g_reg->getpc());
}
