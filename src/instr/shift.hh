#ifndef SHIFT_HH_
# define SHIFT_HH_

# include <stdexcept>
# include <boost/cstdint.hpp>

# include <env/registers.hh>
# include <instr/bitwise.hh>
# include <debug.hh>


namespace Shift
{
    void sll(uint32_t ins);
    void sllv(uint32_t ins);

    void srl(uint32_t ins);
    void srlv(uint32_t ins);

    void sra(uint32_t ins);
    void srav(uint32_t ins);
} // Shift


#endif /* !SHIFT_HH_ */
