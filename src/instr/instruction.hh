#ifndef INSTRUCTION_HH_
# define INSTRUCTION_HH_

# include <map>
# include <iostream>
# include <boost/cstdint.hpp>

# include <instr/bitwise.hh>
# include <debug.hh>

# include <instr/branch.hh>
# include <instr/arith.hh>
# include <instr/logic.hh>
# include <instr/shift.hh>
# include <instr/store.hh>


// instruction identifiers: opcode << 8 + functioncode (0 if not r-type)
typedef enum
{
    // misc
    SYS = 0x000C, // 0000 0000 0000 1100

    // arithmetic instructions
    ADD = 0x0020, // 0000 0000 0010 0000
    ADDU = 0x0021, // 0000 0000 0010 0001
    ADDI = 0x0800, // 0000 1000 0000 0000
    ADDIU = 0x0900, // 0000 1001 0000 0000
    DIV = 0x001A, // 0000 0000 0001 1010
    DIVU = 0x001B, // 0000 0000 0001 1011
    MULT = 0x0018, // 0000 0000 0001 1000
    MULTU = 0x0019, // 0000 0000 0001 1001
    SUB = 0x0022, // 0000 0000 0010 0010
    SUBU = 0x0023, // 0000 0000 0010 0011

    // logic instructions
    AND = 0x0024, // 0000 0000 0010 0100
    ANDI = 0x0C00, // 0000 1100 0000 0000
    NOR = 0x0027, // 0000 0000 0010 0111
    OR = 0x0025, // 0000 0000 0010 0101
    ORI = 0x0D00, // 0000 1101 0000 0000
    SLT = 0x002A, // 0000 0000 0010 1010
    SLTU = 0x002B, // 0000 0000 0010 1011
    SLTI = 0x0A00, // 0000 1010 0000 0000
    SLTIU = 0x0B00, // 0000 1011 0000 0000
    XOR = 0x0026, // 0000 0000 0010 0110
    XORI = 0x0E00, // 0000 1110 0000 0000

    // shift instructions
    SLL = 0x0000, // 0000 0000 0000 0000
    SLLV = 0x0004, // 0000 0000 0000 0100
    SRL = 0x0002, // 0000 0000 0000 0010
    SRLV = 0x0006, // 0000 0000 0000 0110
    SRA = 0x0003, // 0000 0000 0000 0011
    SRAV = 0x0007, // 0000 0000 0000 0111

    // load and stores
    LB = 0x2000, // 0010 0000 0000 0000
    LBU = 0x2400, // 0010 0100 0000 0000
    LH = 0x2100, // 0010 0001 0000 0000
    LHU = 0x2500, // 0010 0101 0000 0000
    LW = 0x2300, // 0010 0011 0000 0000
    SB = 0x2800, // 0010 1000 0000 0000
    SH = 0x2900, // 0010 1001 0000 0000
    SW = 0x2B00, // 0010 1011 0000 0000
    LUI = 0x0F00, // 0000 1111 000 0000
    MFLO = 0x0012, // 0000 0000 0001 0010
    MFHI = 0x0010, // 0000 0000 0001 0000
    MTLO = 0x0013, // 0000 0000 0001 0011
    MTHI = 0x0011, // 0000 0000 0001 0001

    // jump instructions
    JALR = 0x0024, // 0000 0000 0010 0100
    JAL = 0x0300, // 0000 0011 0000 0000
    JR = 0x0008, // 0000 0000 0000 1000
    J = 0x0200, // 0000 0010 0000 0000
    BEQ = 0x0400, // 0000 0100 0000 0000
    BNE = 0x0500, // 0000 0101 0000 0000
    REGIMM = 0x0100, // 0000 0001 0000 0000
    BGTZ = 0x0700, // 0000 0111 0000 0000 0000
    BLEZ = 0x0600, // 0000 0110 0000 0000
} e_instr;


typedef void (*f_instr)(uint32_t);


class Instruction
{
    public:
    Instruction(uint32_t instruction, uint32_t instr_offset);
    void execute();
    bool delayslot() const;

    private:
    // instruction set, pair an "identifier" with a function instruction
    static std::map<e_instr, f_instr> instrset;

    uint32_t instru;
    uint32_t ofs;
    e_instr ident;
};


#endif /* !INSTRUCTION_HH_ */
