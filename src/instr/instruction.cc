#include <instr/instruction.hh>


std::map<e_instr, f_instr> Instruction::instrset =
{
    // misc
    { SYS, syscall },

    // arithmetics
    { ADD, Arith::add },
    { ADDU, Arith::addu },
    { ADDI, Arith::addi },
    { ADDIU, Arith::addiu },
    { DIV, Arith::div },
    { DIVU, Arith::divu },
    { MULT, Arith::mult },
    { MULTU, Arith::multu },
    { SUB, Arith::sub },
    { SUBU, Arith::subu },

    // logic, bitwise
    { ORI, Logic::ori },
    { AND, Logic::and_instr },
    { ANDI, Logic::andi },
    { NOR, Logic::nor },
    { OR, Logic::or_instr },
    { SLT, Logic::slt },
    { SLTU, Logic::sltu },
    { SLTI, Logic::slti },
    { SLTIU, Logic::sltiu },
    { XOR, Logic::xor_instr },
    { XORI, Logic::xori },

    // shifts
    { SLL, Shift::sll },
    { SLLV, Shift::sllv },
    { SRL, Shift::srl },
    { SRLV, Shift::srlv },

    // load and stores
    { LB, Store::lb },
    { LBU, Store::lbu },
    { LH, Store::lh },
    { LHU, Store::lhu },
    { LW, Store::lw },
    { SB, Store::sb },
    { SH, Store::sh },
    { SW, Store::sw },
    { LUI, Store::lui },
    { MFLO, Store::mflo },
    { MFHI, Store::mfhi },
    { MTLO, Store::mtlo },
    { MTHI, Store::mthi },

    // jump and branches
    { JALR, Branch::jalr },
    { JAL, Branch::jal },
    { JR, Branch::jr },
    { J, Branch::j },
    { BEQ, Branch::beq },
    { BNE, Branch::bne },
    { REGIMM, Branch::regimm },
    { BGTZ, Branch::bgtz },
    { BLEZ, Branch::blez }
};


/*
** an instruction is identified by its "identifier," which is its
** opcode << 8 plus its function code if it is an r-type instruction,
** otherwise 0, and then cast it to its enumeration equivalent.
*/
Instruction::Instruction(uint32_t instruction, uint32_t instr_offset) :
    instru(instruction), ofs(instr_offset)
{
    int identifier = GET_OPCODE(this->instru) << 8;

    // this is a r-type instruction, we add the "function" field
    if (identifier == 0)
        identifier += GET_FC(this->instru);

    this->ident = (e_instr)identifier;
}


void
Instruction::execute()
{
    f_instr to_exec(Instruction::instrset[this->ident]);

    // print registers if --print-registers
    if (DBG_REG(g_debug))
        std::cerr << *g_reg << std::endl;

    // print instruction if --debug
    if (DBG_INS(g_debug))
        fprintf(stderr, DBG_MSG "0x%.8X: 0x%.8X: ", this->ofs, this->instru);

    if (to_exec != 0x0)
        to_exec(this->instru);
    else if (DBG_INS(g_debug))
        fprintf(stderr, "invalid\n");
}


// indicates wether or not the instructions needs to execute the delay slot
// prior to executing itself
bool
Instruction::delayslot() const
{
    return this->ident == JAL || this->ident == J || this->ident == JR ||
        this->ident == BEQ || this->ident == BEQ || this->ident == BNE ||
        this->ident == REGIMM || this->ident == BGTZ || this->ident == BLEZ;
}
