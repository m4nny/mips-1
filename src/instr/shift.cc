#include <instr/shift.hh>


void
Shift::sll(uint32_t ins)
{
    if (DBG_INS(g_debug))
    {
        // clarity purpose when reading debug
        if (ins == 0)
            fprintf(stderr, "nop\n");
        else
            fprintf(stderr, "sll $%u, $%u, $%u\n", GRD(ins), GRT(ins),
                GSA(ins));
    }

    g_reg->set(GRD(ins), g_reg->get(GRT(ins)) << GSA(ins));
}


void
Shift::sllv(uint32_t ins)
{
    if (DBG_INS(g_debug))
        fprintf(stderr, "sllv $%u, $%u, $%u\n", GRD(ins), GRT(ins), GRS(ins));

    g_reg->set(GRD(ins),
        g_reg->get(GRT(ins)) << (g_reg->get(GRS(ins)) & 0x1F));
}


void
Shift::srl(uint32_t ins)
{
    if (DBG_INS(g_debug))
        fprintf(stderr, "srl $%u, $%u, $%u\n", GRD(ins), GRT(ins), GSA(ins));

    g_reg->set(GRD(ins), g_reg->get(GRT(ins)) >> GSA(ins));
}


void
Shift::srlv(uint32_t ins)
{
    if (DBG_INS(g_debug))
        fprintf(stderr, "srlv $%u, $%u, $%u\n", GRD(ins), GRT(ins), GRS(ins));

    g_reg->set(GRD(ins),
        g_reg->get(GRT(ins)) >> (g_reg->get(GRS(ins)) & 0x1F));
}
