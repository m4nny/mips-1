#include <instr/logic.hh>


void
Logic::and_instr(uint32_t ins)
{
    if (DBG_INS(g_debug))
        fprintf(stderr, "and $%u, $%u, $%u\n", GRD(ins), GRS(ins), GRT(ins));

    g_reg->set(GRD(ins), g_reg->get(GRS(ins)) & g_reg->get(GRT(ins)));
}


void
Logic::andi(uint32_t ins)
{
    if (DBG_INS(g_debug))
        fprintf(stderr, "andi $%u, $%u, $%u\n", GRD(ins), GRS(ins), GIM(ins));

    g_reg->set(GRT(ins), g_reg->get(GRS(ins)) & (uint16_t)GIM(ins));
}


void
Logic::nor(uint32_t ins)
{
    if (DBG_INS(g_debug))
        fprintf(stderr, "nor $%u, $%u, $%u\n", GRD(ins), GRS(ins), GRT(ins));

    g_reg->set(GRD(ins), ~(g_reg->get(GRS(ins)) | g_reg->get(GRT(ins))));
}


void
Logic::or_instr(uint32_t ins)
{
    if (DBG_INS(g_debug))
        fprintf(stderr, "or $%u, $%u, $%u\n", GRD(ins), GRS(ins), GRT(ins));

    g_reg->set(GRD(ins), g_reg->get(GRS(ins)) | g_reg->get(GRT(ins)));
}


void
Logic::ori(uint32_t ins)
{
    if (DBG_INS(g_debug))
        fprintf(stderr, "ori $%u, $%u, 0x%X\n", GRT(ins), GRS(ins), GIM(ins));

    g_reg->set(GRT(ins), g_reg->get(GRS(ins)) | (uint16_t)GIM(ins));
}


void
Logic::slt(uint32_t ins)
{
    if (DBG_INS(g_debug))
        fprintf(stderr, "slt $%u, $%u, $%u\n", GRD(ins), GRS(ins), GRT(ins));

    g_reg->set(GRD(ins), g_reg->get(GRS(ins)) < g_reg->get(GRT(ins)));
}


void
Logic::sltu(uint32_t ins)
{
    if (DBG_INS(g_debug))
        fprintf(stderr, "sltu $%u, $%u, $%u\n", GRD(ins), GRS(ins), GRT(ins));

    g_reg->set(GRD(ins),
        (uint32_t)g_reg->get(GRS(ins)) < (uint32_t)g_reg->get(GRT(ins)));
}


void
Logic::slti(uint32_t ins)
{
    if (DBG_INS(g_debug))
        fprintf(stderr, "slti $%u, $%u, 0x%X\n", GRT(ins), GRS(ins), GIM(ins));

    g_reg->set(GRT(ins), g_reg->get(GRS(ins)) < (int32_t)(int16_t)GIM(ins));
}


void
Logic::sltiu(uint32_t ins)
{
    if (DBG_INS(g_debug))
        fprintf(stderr, "slti $%u, $%u, 0x%X\n", GRT(ins), GRS(ins), GIM(ins));

    g_reg->set(GRT(ins),
        (uint32_t)g_reg->get(GRS(ins)) < (uint32_t)(uint16_t)GIM(ins));
}


void
Logic::xor_instr(uint32_t ins)
{
    if (DBG_INS(g_debug))
        fprintf(stderr, "xor $%u, $%u, $%u\n", GRD(ins), GRS(ins), GRT(ins));

    g_reg->set(GRD(ins), g_reg->get(GRS(ins)) ^ g_reg->get(GRT(ins)));
}


void
Logic::xori(uint32_t ins)
{
    if (DBG_INS(g_debug))
        fprintf(stderr, "xori $%u, $%u, 0x%X\n", GRT(ins), GRS(ins), GIM(ins));

    g_reg->set(GRT(ins), g_reg->get(GRS(ins)) ^ (uint16_t)GIM(ins));
}
