#ifndef ARITH_HH_
# define ARITH_HH_

# include <stdexcept>
# include <boost/cstdint.hpp>

# include <env/registers.hh>
# include <instr/bitwise.hh>
# include <instr/syscalls.hh>
# include <debug.hh>


namespace Arith
{
    void add(uint32_t ins);
    void addu(uint32_t ins);
    void addi(uint32_t ins);
    void addiu(uint32_t ins);

    void div(uint32_t ins);
    void divu(uint32_t ins);

    void mult(uint32_t ins);
    void multu(uint32_t ins);

    void sub(uint32_t ins);
    void subu(uint32_t ins);
} // Arith


#endif /* !ARITH_HH_ */
