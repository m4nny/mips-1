#include <string>
#include <stdexcept>
#include <iostream>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>

#include <run.hh>
#include <env/memory.hh>
#include <env/registers.hh>

#define FILE_NOT_FOUND  "file not found"
#define ERROR_PROMPT    MYMIPS_PROMPT "error: "


namespace fs = boost::filesystem;
namespace po = boost::program_options;


int g_debug = 0;


static std::string
parse_args(int ac, char const* av[])
{
    // setup parsing
    po::positional_options_description p;
    p.add("binary", -1);
    po::options_description dsc("options");
    dsc.add_options() ("help", "produce this message")
        ("debug", "enable debug outputs")
        ("print-registers", "print registers content")
        ("binary", po::value<std::string>(), "binary file to execute");

    // parse arguments
    po::variables_map vm;
    po::store(po::command_line_parser(ac, av).options(dsc).positional(p).run(),
        vm);

    po::notify(vm);

    // help?
    if (vm.count("help"))
    {
        std::cout << dsc;
        return std::string("");
    }

    // analyzing parsed args
    if (vm.count("debug"))
        g_debug += 1;
    if (vm.count("print-registers"))
        g_debug += 2;
    if (vm.count("binary") == 0 ||
        !fs::exists(vm["binary"].as<std::string>().c_str()))
        throw std::logic_error(FILE_NOT_FOUND);

    // cannot directly return const char* because of vm's scope
    return vm["binary"].as<std::string>();
}


int
main(int argc, char const* argv[])
{
    unsigned int status(0);

    try
    {
        // if parse_args returns an empty string, it just displayed the help
        std::string binaryname(parse_args(argc, argv));
        if (binaryname.empty())
            return 0;

        // initialization of the environment
        g_mem = new Memory(binaryname.c_str());
        g_reg = new Registers();

        status = run();
    }
    catch (std::exception& e)
    {
        std::cerr << ERROR_PROMPT << e.what() << std::endl;
        status = 1;
    }

    if (g_mem != 0x0)
        delete g_mem;

    if (g_reg != 0x0)
        delete g_reg;

    return status;
}
