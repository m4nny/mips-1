#ifndef ELFPARSING_HH_
# define ELFPARSING_HH_

# include <elf.h>
# include <fstream>
# include <stdexcept>

# define ELF_ERR    "invalid MIPS elf"


class ELF
{
    public:
    ELF(std::ifstream& input);
    static bool iself(std::ifstream& input);
    ~ELF();

    bool next_segment(uint32_t* virtual_addr, uint32_t* real_addr,
        uint32_t* size, std::ifstream& input);

    private:
    Elf32_Ehdr* header;
    Elf32_Phdr* ph_entry;

    // offset of the next program header to read
    std::streampos cur_offset;
};


#endif /* !ELFPARSING_HH_ */
