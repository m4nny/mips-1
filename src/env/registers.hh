#ifndef REGISTERS_HH_
# define REGISTERS_HH_

# include <iomanip>
# include <iostream>
# include <stdexcept>
# include <boost/cstdint.hpp>

# include <env/memory.hh>
# include <debug.hh>


// registre id, ZERO is $0, A0 is $4, RA is $31, etc
typedef enum
{
    ZERO, AT,
    V0, V1,
    A0, A1, A2, A3, A4, A5, A6, A7,
    T4, T5, T6, T7,
    S0, S1, S2, S3, S4, S5, S6, S7,
    T8, T9,
    K0, K1,
    GP, SP, S8, RA,
} e_regid;


class Registers
{
    public:
    Registers();
    ~Registers();

    // use only with e_regid or unsigned int
    template<class T = e_regid>
    int get(T id) const;

    // use only with e_regid or unsigned int
    template<class T = e_regid>
    void set(T id, int value);

    uint32_t getpc() const;
    void setpc(uint32_t value);
    void incpc();

    int gethi() const;
    void sethi(int value);

    int getlo() const;
    void setlo(int value);

    friend std::ostream& operator<<(std::ostream& out, const Registers& top);

    private:
    int32_t* grid;
    int32_t hi;
    int32_t lo;
    uint32_t pc;
};


extern const uint32_t g_reg_nbr;
extern Registers* g_reg;


# include <env/registers.hxx>

#endif /* !REGISTERS_HH_ */
