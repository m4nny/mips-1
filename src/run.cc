#include <run.hh>


// Fetch the next instruction and increment the program counter
static uint32_t
fetch()
{
    return TO_INT(g_mem->get(g_reg->getpc()));
}


// decode the instruction and returns a pointer to Instruction object
static Instruction*
decode(uint32_t instrcode)
{
    return new Instruction(instrcode, g_reg->getpc());
}


// fetch, decode and execute instructions until the end
int
run()
{
    Instruction* current(0x0);
    Instruction* delayinstr(0x0);

    try
    {
        while (true)
        {
            current = decode(fetch());
            g_reg->incpc();

            // execute the delay slot if there is any
            if (current->delayslot())
            {
                delayinstr = decode(fetch());
                delayinstr->execute();

                delete delayinstr;
                delayinstr = 0x0;
            }

            current->execute();
            delete current;
        }
    }
    // clean exit on syscall handling
    catch (Exit_Signal& e)
    {
        if (current != 0x0)
            delete current;

        if (delayinstr != 0x0)
            delete delayinstr;

        return e.returncode();
    }

    return 0;
}
